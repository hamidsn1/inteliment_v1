// Generated code from Butter Knife. Do not modify!
package inteliment.h_sed.example.com.ilteliment;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class MainActivity$$ViewInjector {
  public static void inject(Finder finder, final inteliment.h_sed.example.com.ilteliment.MainActivity target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131492955, "field 'englishButton' and method 'onNextClick'");
    target.englishButton = (android.widget.ImageButton) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.onNextClick(p0);
        }
      });
    view = finder.findRequiredView(source, 2131492956, "field 'frenchButton' and method 'onNextClick'");
    target.frenchButton = (android.widget.ImageButton) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.onNextClick(p0);
        }
      });
    view = finder.findRequiredView(source, 2131492957, "field 'chineseButton' and method 'onNextClick'");
    target.chineseButton = (android.widget.ImageButton) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.onNextClick(p0);
        }
      });
    view = finder.findRequiredView(source, 2131492958, "field 'spanishButton' and method 'onNextClick'");
    target.spanishButton = (android.widget.ImageButton) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.onNextClick(p0);
        }
      });
    view = finder.findRequiredView(source, 2131492959, "field 'germanButton' and method 'onNextClick'");
    target.germanButton = (android.widget.ImageButton) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.onNextClick(p0);
        }
      });
    view = finder.findRequiredView(source, 2131492946, "field 'pager'");
    target.pager = (inteliment.h_sed.example.com.ilteliment.ClickableViewPager) view;
    view = finder.findRequiredView(source, 2131492948, "field 'fourthText'");
    target.fourthText = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131492951, "field 'blue_btn'");
    target.blue_btn = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492950, "field 'red_btn'");
    target.red_btn = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492952, "field 'green_btn'");
    target.green_btn = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131492943, "field 'myBackLayout'");
    target.myBackLayout = (android.widget.RelativeLayout) view;
  }

  public static void reset(inteliment.h_sed.example.com.ilteliment.MainActivity target) {
    target.englishButton = null;
    target.frenchButton = null;
    target.chineseButton = null;
    target.spanishButton = null;
    target.germanButton = null;
    target.pager = null;
    target.fourthText = null;
    target.blue_btn = null;
    target.red_btn = null;
    target.green_btn = null;
    target.myBackLayout = null;
  }
}
